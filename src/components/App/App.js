import React, { Component } from 'react';
import './App.scss';
import { Switch, Route, Link, withRouter } from 'react-router-dom';
import ListCars from '../ListCars/ListCars';
import ListChandeliers from '../ListChandeliers/ListChandeliers';
import withName from '../withName/withName';
// import { Fragment } from 'react';

 class App extends Component {   
  render() {
    const ListChandeliersWithName = withName(ListChandeliers, 'люстры');
    return (
      <main>

        <div className='wrap'>
          <div className='wrap__header'>Типы машин и люстр</div>
          <div className='wrap__message'>
            <div className='wrap__links'>
              <Link className='wrap__links_cars' to="/ListCars">Машины </Link>
              <Link className='wrap__links_chandeliers'to="/ListChandeliers"> Люстры</Link>
            <div className='wrap-text'>
              <Switch>
                <Route path="/ListCars">
                  <ListCars />
                </Route>
                <Route path="/ListChandeliers">
                  <ListChandeliersWithName />
                </Route>
                {/* <Route path='*'>
                  <div>ne tot url</div>
                </Route> */}
              </Switch>
            </div>
            </div>
          </div>    
        </div>

        

      </main>
    );
  }
}

export default withRouter(App);

