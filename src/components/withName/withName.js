import React, { Component } from 'react';

const withName = (WrappedComponent, name) => {
  
  class HOC extends Component {
    componentDidMount() {
      console.log(name);
    }

    render() {
      return <WrappedComponent {...this.props} />;
    }
  }
  return HOC;
};

export default withName;
