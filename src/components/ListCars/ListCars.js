import { Component } from 'react';
import  withName  from '../withName/withName';

class ListCars extends Component {
    render() {  
      
      return (
        <ul>
          <li>седан</li>
          <li>универсал</li>
          <li>лимузин</li>
        </ul>
      )
    }
  }

export default withName(ListCars, 'машины')
